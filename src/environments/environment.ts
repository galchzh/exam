// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: "AIzaSyARNJNNhft8JEOxtPmUrJxa9V4goxbm7Zw",
    authDomain: "exam-17587.firebaseapp.com",
    databaseURL: "https://exam-17587.firebaseio.com",
    projectId: "exam-17587",
    storageBucket: "",
    messagingSenderId: "262418883495"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
